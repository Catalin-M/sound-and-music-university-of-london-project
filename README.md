This project was built using Spring Boot, Thymeleaf and Bootstrap. I also used Azure Blob Storage to store audio files.  

The project wasn't a lot of development time, but the goal was to see if users would perceive music differently when the context changes, as in, does a Christmas carol sound different if the background and the overall context suggests winter vs summer.  

The code had the URLs to the songs renamed but if you're curious how it would run, replace the URLs inside the code with your own URLs.  

A better approach would have been to programmatically retrieve the list of songs and update the file names dynamically based on the responses.  

Added here simply to keep a record of the code itself.
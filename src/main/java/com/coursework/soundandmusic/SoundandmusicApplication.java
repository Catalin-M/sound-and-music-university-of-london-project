package com.coursework.soundandmusic;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SoundandmusicApplication {

	public static void main(String[] args) {
		SpringApplication.run(SoundandmusicApplication.class, args);
	}
}

/**
 * The HomeController completes the work to respond to any requests received to the different 
 * endpoints. 
 * 
 * It also uses autowiring to initialize season objects which are then used throughout the class.
 */

package com.coursework.soundandmusic.controller;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.List;

import com.coursework.soundandmusic.entity.*;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class HomeController {

    @Autowired
    Autumn autumn;
    @Autowired
    Summer summer;
    @Autowired
    Spring spring;
    @Autowired
    Winter winter;

    public HomeController(){}

    // when a user arrives at the root of the page the index.html file is returned
    @RequestMapping("/")
    public String homePage() {
        return "index";
    }

    // when a user clicks on the winter button, the /winter endpoint is called
    // a model object is initialized and a winter object is added as an attribute to the model
    // this allows Thymeleaf to call the properties of the object that was bound to the model
    // after that, the winter.html page is returned
    @RequestMapping("/winter")
    public String winterPage(Model theModel) {        
        theModel.addAttribute("winter", winter);
        return "winter";
    }

    // same as for the winter endpoint, only here we bind a spring object to the model
    @RequestMapping("/spring")
    public String springPage(Model theModel) {
        theModel.addAttribute("spring", spring);
        return "spring";
    }

    // same as for the winter endpoint, only here we bind a summer object to the model
    @RequestMapping("/summer")
    public String summerPage(Model theModel) {        
        theModel.addAttribute("summer", summer);
        return "summer";
    }

    // same as for the winter endpoint, only here we bind an autumn object to the model
    @RequestMapping("/autumn")
    public String autumnPage(Model theModel) {    
        theModel.addAttribute("autumn", autumn);
        return "autumn";
    }     

    // this method listens for calls to songs/winter/{songId}
    // songId is a string which is a "shortcut" to a given path - it mirrors the way key
    // value pair objects work
    // the request is sent by the audio control, so the output to this is a stream
    // the id gets passed in as a path variable, so even without the control, one could visit
    // the full url with /songs/winter/songId as long as you know the id, to listesn to the song
    // the song id is passed in, and the method loops through the songs for teh given season
    // returning the one where the id matches otherwise it returns null.
    // the process is the same for all seasons:
    // 1. User clicks play - the song id is mapped to the <audio controls> tag
    // 2. Song id is passed through in the path as a variable which is then mapped to a local var
    // 3. 
    @GetMapping(value = "/songs/winter/{songId}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public @ResponseBody byte[] getWinterAudio(@PathVariable("songId") String songId) throws IOException {
        
        List<Song> songList = winter.getSongList();
        for (Song s : songList) {
            if (s.getSongId().equals(songId)) {
                InputStream song = new URL(s.getPath()).openStream();
                return IOUtils.toByteArray(song);
            }            
        }
        return null;
    }

    @GetMapping(value = "/songs/spring/{songId}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public @ResponseBody byte[] getSpringAudio(@PathVariable("songId") String songId) throws IOException {
        
        List<Song> songList = spring.getSongList();
        for (Song s : songList) {
            if (s.getSongId().equals(songId)) {
                InputStream song = new URL(s.getPath()).openStream();
                return IOUtils.toByteArray(song);
            }            
        }        
        return null;
    }

    @GetMapping(value = "/songs/summer/{songId}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public @ResponseBody byte[] getSummerAudio(@PathVariable("songId") String songId) throws IOException {
        
        List<Song> songList = summer.getSongList();
        for (Song s : songList) {
            if (s.getSongId().equals(songId)) {
                InputStream song = new URL(s.getPath()).openStream();
                return IOUtils.toByteArray(song);
            }            
        }        
        return null;
    }

    @GetMapping(value = "/songs/autumn/{songId}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public @ResponseBody byte[] getAutumnAudio(@PathVariable("songId") String songId) throws IOException {
        
        List<Song> songList = autumn.getSongList();
        for (Song s : songList) {
            if (s.getSongId().equals(songId)) {
                InputStream song = new URL(s.getPath()).openStream();
                return IOUtils.toByteArray(song);
            }            
        }
        return null;
    }
}
package com.coursework.soundandmusic.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public class Summer {
    private Map<Integer, String> backgroundFiles = new HashMap<Integer, String>();
    private Map<Integer, String> text = new HashMap<Integer, String>();
    private List<Song> songList = new ArrayList<Song>();

    public Summer() {
        backgroundFiles.put(1, "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/summer_germany.jpeg");
        backgroundFiles.put(2, "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/summer_japan.jpg");
        backgroundFiles.put(3, "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/summer_canada.jpg");
        backgroundFiles.put(4, "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/summer_russia.jpg");

        text.put(1, "*Some of the most popular songs in Germany during summer (according to basic Google Searches).");
        text.put(2, "*Some of the most popular songs in Japan during summer (according to basic Google Searches).");
        text.put(3, "*Some of the most popular songs in Canada during summer (according to basic Google Searches).");
        text.put(4, "*Some of the most popular songs in Russia during summer (according to basic Google Searches).");        

        //songList.add(new Song("sumg1", "src/main/resources/static/99_luftballons.aac"));
        // 
        songList.add(new Song("sumg1", "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/99_luftballons.aac"));
        songList.add(new Song("sumg2", "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/what_is_love.aac"));
        songList.add(new Song("sumg3", "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/wind_of_change.aac")); //

        songList.add(new Song("sumj1", "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/summer_festival.aac"));
        songList.add(new Song("sumj2", "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/bokura_no_eureka.aac"));
        songList.add(new Song("sumj3", "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/hot_limit.aac"));

        songList.add(new Song("sumc1", "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/born_to_be_wild.aac"));
        songList.add(new Song("sumc2", "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/summer_of_69.aac"));
        songList.add(new Song("sumc3", "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/hand_in_my_pocket.aac"));

        songList.add(new Song("sumr1", "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/pink_flamingo.aac"));
        songList.add(new Song("sumr2", "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/baikal.aac"));
        songList.add(new Song("sumr3", "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/18_mne_uzhe.aac"));       
    }

    public Map<Integer, String> getBackgroundFiles() {
        return backgroundFiles;
    }

    public void setBackgroundFiles(Map<Integer, String> backgroundFiles) {
        this.backgroundFiles = backgroundFiles;
    }

    public Map<Integer, String> getText() {
        return text;
    }

    public void setText(Map<Integer, String> text) {
        this.text = text;
    }

    public List<Song> getSongList() {
        return songList;
    }

    public void setSongList(List<Song> songList) {
        this.songList = songList;
    }
}

package com.coursework.soundandmusic.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public class Spring {
    private Map<Integer, String> backgroundFiles = new HashMap<Integer, String>();
    private Map<Integer, String> text = new HashMap<Integer, String>();
    private List<Song> songList = new ArrayList<Song>();

    public Spring() {
        backgroundFiles.put(1, "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/spring_germany.jpeg");
        backgroundFiles.put(2, "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/spring_japan.jpg");
        backgroundFiles.put(3, "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/spring_canada.jpg");
        backgroundFiles.put(4, "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/spring_russia.jpg");

        text.put(1, "*Some of the most popular songs in Germany during spring (according to basic Google Searches).");
        text.put(2, "*Some of the most popular songs in Japan during spring (according to basic Google Searches).");
        text.put(3, "*Some of the most popular songs in Canada during spring (according to basic Google Searches).");
        text.put(4, "*Some of the most popular songs in Russia during spring (according to basic Google Searches).");        

        songList.add(new Song("sprg1", "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/fruhlingslied.aac"));
        songList.add(new Song("sprg2", "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/die_mainacht.aac"));
        songList.add(new Song("sprg3", "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/christ_ist_erstanden.aac"));

        songList.add(new Song("sprj1", "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/sakura_sakura.aac"));
        songList.add(new Song("sprj2", "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/haru_yo_koi.aac"));
        songList.add(new Song("sprj3", "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/ashita_haru_ga_kitara.aac"));

        songList.add(new Song("sprc1", "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/canada_in_springtime.aac"));
        songList.add(new Song("sprc2", "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/month_of_may.aac"));
        songList.add(new Song("sprc3", "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/the_magician.aac"));

        songList.add(new Song("sprr1", "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/rite_of_spring.aac"));
        songList.add(new Song("sprr2", "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/as_life_without_spring.aac"));
        songList.add(new Song("sprr3", "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/rachmanino_spring.mp3"));
    }

    public Map<Integer, String> getBackgroundFiles() {
        return backgroundFiles;
    }

    public void setBackgroundFiles(Map<Integer, String> backgroundFiles) {
        this.backgroundFiles = backgroundFiles;
    }

    public Map<Integer, String> getText() {
        return text;
    }

    public void setText(Map<Integer, String> someText) {
        this.text = someText;
    }
    
    public List<Song> getSongList() {
        return songList;
    }

    public void setSongList(List<Song> songList) {
        this.songList = songList;
    }
}

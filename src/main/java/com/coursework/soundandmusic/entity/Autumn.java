package com.coursework.soundandmusic.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public class Autumn {
    private Map<Integer, String> backgroundFiles = new HashMap<Integer, String>();
    private Map<Integer, String> text = new HashMap<Integer, String>();
    private List<Song> songList = new ArrayList<Song>();

    public Autumn() {
        backgroundFiles.put(1, "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/autumn_germany.jpeg");
        backgroundFiles.put(2, "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/autumn_japan.jpg");
        backgroundFiles.put(3, "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/autumn_canada.jpg");
        backgroundFiles.put(4, "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/autumn_russia.jpg");

        text.put(1, "*Some of the most popular songs in Germany during autumn (according to basic Google Searches).");
        text.put(2, "*Some of the most popular songs in Japan during autumn (according to basic Google Searches).");
        text.put(3, "*Some of the most popular songs in Canada during autumn (according to basic Google Searches).");
        text.put(4, "*Some of the most popular songs in Russia during autumn (according to basic Google Searches).");        

        songList.add(new Song("autg1", "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/ein_prosit.aac"));
        songList.add(new Song("autg2", "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/schubert_autumn.aac"));
        songList.add(new Song("autg3", "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/hande_zum_himmel.aac"));

        songList.add(new Song("autj1", "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/japanese_autumn.aac"));
        songList.add(new Song("autj2", "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/crescent_moon.aac"));
        songList.add(new Song("autj3", "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/cosmos.aac"));

        songList.add(new Song("autc1", "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/october_road.aac"));
        songList.add(new Song("autc2", "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/harvest_moon.aac"));
        songList.add(new Song("autc3", "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/falling_leaves.aac"));

        songList.add(new Song("autr1", "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/kalinka.aac"));
        songList.add(new Song("autr2", "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/all_the_leaves_are_yellow.aac"));
        songList.add(new Song("autr3", "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/what_is_autumn.aac"));       
    }

    public Map<Integer, String> getBackgroundFiles() {
        return backgroundFiles;
    }

    public void setBackgroundFiles(Map<Integer, String> backgroundFiles) {
        this.backgroundFiles = backgroundFiles;
    }

    public Map<Integer, String> getText() {
        return text;
    }

    public void setText(Map<Integer, String> text) {
        this.text = text;
    }

    public List<Song> getSongList() {
        return songList;
    }

    public void setSongList(List<Song> songList) {
        this.songList = songList;
    }
}

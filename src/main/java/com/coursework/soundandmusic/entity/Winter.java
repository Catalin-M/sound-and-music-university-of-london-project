package com.coursework.soundandmusic.entity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Component;

@Component
public class Winter {
    private Map<Integer, String> backgroundFiles = new HashMap<Integer, String>();
    private Map<Integer, String> text = new HashMap<Integer, String>();
    private List<Song> songList = new ArrayList<Song>();
    
    public Winter() {
        // the background for each country
        backgroundFiles.put(1, "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/winter_germany.jpeg");
        backgroundFiles.put(2, "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/winter_japan.jpg");
        backgroundFiles.put(3, "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/winter_canada.jpg");
        backgroundFiles.put(4, "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/winter_russia.jpg");
        
        text.put(1, "*Some of the most popular songs in Germany during winter (according to basic Google Searches).");
        text.put(2, "*Some of the most popular songs in Japan during summer (according to basic Google Searches).");
        text.put(3, "*Some of the most popular songs in Canada during summer (according to basic Google Searches).");
        text.put(4, "*Some of the most popular songs in Russia during winter (according to basic Google Searches).");  
        
        // song playlist with wing being winter germany songs
        // winj being winter japan songs
        // winc being winter canada songs
        // wini being winter russia songs
        songList.add(new Song("wing1", "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/silent_night.mp3"));
        songList.add(new Song("wing2", "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/o_du_frohliche.aac"));
        songList.add(new Song("wing3", "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/frosty_der_schneemann.mp3"));

        songList.add(new Song("winj1", "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/yuki_no_hana.aac"));
        songList.add(new Song("winj2", "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/suteki_na_holiday.aac"));
        songList.add(new Song("winj3", "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/silent_eve.aac"));

        songList.add(new Song("winc1", "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/wintersong.aac"));
        songList.add(new Song("winc2", "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/let_it_snow.aac"));
        songList.add(new Song("winc3", "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/auld_lang_syne.aac"));

        songList.add(new Song("winr1", "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/valenki.aac"));
        songList.add(new Song("winr2", "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/misty_morning.aac"));
        songList.add(new Song("winr3", "_yourblobstorageurlhere_.blob.core.windows.net/soundandmusicstorage/scriabin.aac"));
    }

    public Map<Integer, String> getBackgroundFiles() {
        return backgroundFiles;
    }

    public void setBackgroundFiles(Map<Integer, String> backgroundFiles) {
        this.backgroundFiles = backgroundFiles;
    }

    public Map<Integer, String> getText() {
        return text;
    }

    public void setText(Map<Integer, String> someText) {
        this.text = someText;
    }

    public List<Song> getSongList() {
        return songList;
    }

    public void setSongList(List<Song> songList) {
        this.songList = songList;
    }
}

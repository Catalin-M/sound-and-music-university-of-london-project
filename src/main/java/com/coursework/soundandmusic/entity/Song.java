package com.coursework.soundandmusic.entity;


public class Song {
     private String songId;
     private String path;     

     public Song() {}

     public Song(String songId, String path) {
         this.songId = songId;
         this.path = path;         
     }

     public String getSongId() {
         return songId;
     }

     public void setSongId(String songId) {
         this.songId = songId;
     }

     public String getPath() {
         return path;
     }

     public void setPath(String path) {
         this.path = path;
     }
}